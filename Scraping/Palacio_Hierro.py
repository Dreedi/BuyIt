# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup
from urllib2 import urlopen


def getkeywords():
	# The URL of the Liverpool MX Site 
	url = 'http://www.elpalaciodehierro.com/'
	
	# Get the HTML description of the URL
	html = urlopen(url).read()
	
	# Creating a BeautifulSoup object form the HTML
	soap = BeautifulSoup(html, "lxml")

	# Creating a variable to save the place where we are looking for
	Answer = soap.find(id="nav").contents

	# Creating a list where we will save our result
	keywords = list()

	# Finding the <a> html label in our answer
	for i in range(2, len(Answer) - 3):
		Answer2 = Answer[i].findAll('a')
		for j in Answer2:
			# Taking the <a> label text
			ForNow =  j.text[1:-1]
			# Evading some words
			if ForNow != (u'Tendencias') and ForNow != (u'Otros'):
				keywords.append(ForNow.replace('\n', ''))

	# Convert the list to a set to clear all the repeated elements and then convert it back to list
	keywords = list(set(keywords))
	# Returning or keywords		
	return keywords
