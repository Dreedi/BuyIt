# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup
from urllib2 import urlopen

def getKeywords():

	# The URL of the Liverpool MX Site
	url = 'http://www.walmart.com.mx/Categorias-Todas.aspx'

	# Get the HTML description of the URL
	html = urlopen(url).read()

	# Creating a BeautifulSoup object form the HTML
	soap = BeautifulSoup(html, "lxml")

	# Creating a variable to save the place where we are looking for
	Answer = soap.find(id="CategoriasTodo").contents

	# Finding the <a> html label in our answer
	Answer = Answer[0].findAll('a')

	# Creating a list where we will save our result
	keywords = list()

	# Iterating in every <a>
	for i in Answer:
		# Taking the <a> label text
		ForNow = i.text
		# Changing some letters
		keywords.append(ForNow.replace('-', ' ').lower())

	# Convert the list to a set to clear all the repeated elements and then convert it back to list
	keywords = list(set(keywords))
	# Returning or keywords
	return keywords