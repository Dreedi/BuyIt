# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup
from urllib2 import urlopen
import unicodedata

Liverpool = 'http://www.liverpool.com.mx'
keywords = list()
link = list(list())

def getKeywords():

	# The URL of the Liverpool MX Site 
	url = Liverpool + '/tienda'
	
	# Get the HTML description of the URL
	html = urlopen(url).read()
	
	# Creating a BeautifulSoup object form the HTML
	soap = BeautifulSoup(html, "lxml")

	# Creating a variable to save the place where we are looking for
	Answer = soap.find(id="menu").contents

	keywords = list()

	# Finding the <a> html label in our answer
	for i in range(1, len(Answer) - 1, 2):
		Answer2 = Answer[i].findAll('span')
		for j in Answer2:

			# Taking the <a> label text
			ForNow = j.text
			# Evading some words
			if ForNow[:-1] != (u'Ver todas las secciones') and ForNow != (u"Ver todas las secciones") and ForNow != (u'ver más') and ForNow != (u'Lo Nuevo') and ForNow[1:] != (u'Lo Nuevo') and ForNow != (u'Otros'):
				keywords.append(ForNow.replace('\n', '').lower())
				link.append([ForNow.replace('\n', ''), j.parent.get('href')])
				
	# Convert the list to a set to clear all the repeated elements and then convert it back to list
	keywords = list(set(keywords))
	return keywords

def removeAcents(text):

    s = ''.join((c for c in unicodedata.normalize('NFD',unicode(text)) if unicodedata.category(c) != 'Mn'))
    return s.decode()

def getWantedType():

	url1 = Liverpool + link[0][1]
	html1 = urlopen(url1).read()
	soap1 = BeautifulSoup(html1, 'lxml')
	Answer1 = soap1.find(id="controls-pagination").contents
	return type(Answer1[3])

def getCorrectUrls(links):

	correctUrls = list()
	for urlFirst in links[:-1]:
		url = removeAcents(Liverpool + urlFirst[1])
		print url
		try:
			html = urlopen(url).read()
			soap = BeautifulSoup(html, 'lxml')
			Answer = soap.find(id="controls-pagination").contents

			#||||||||||||||||||||||||||

			if len(Answer) > 3:

				Key_link = Answer[3].get('href')
				LastKey_link = Answer[-2].get('href')
				LastNumber = int(LastKey_link[-1])

				for Page in range(1, LastNumber + 1):
					Page_url = removeAcents(Liverpool + Key_link[:-1] + str(Page))
					print Page_url
					Page_html = urlopen(Page_url).read()
					Page_soap = BeautifulSoup(Page_html, 'lxml')
					Answer2 = Page_soap.find(id="productos").contents

					for number in range(1 , len(Answer2) -1, 2):
						
						Article_link = Answer2[number].a.get('href')
						
						Article_url = removeAcents(Liverpool + Article_link)
						try:
							Article_html = urlopen(Article_url).read()

							print Article_url
							correctUrls.append(Article_url)
						except Exception, e:
							print e
				
			else:
				Answer2 = soap.find(id="productos").contents
				for number in range(1 , len(Answer2) -1, 2):
					
					Article_link = Answer2[number].a.get('href')

					Article_url = removeAcents(Liverpool + Article_link)
					try:
						Article_html = urlopen(Article_url).read()

						print Article_url
						correctUrls.append(Article_url)
					except Exception, e:
						print e

		except IndexError, e:
			print e

		except Exception, e:
			print e

	return correctUrls


def getArticles(links):

	correctUrls = getCorrectUrls(links)

	print 'Articles'
	for Article_url in correctUrls:

		Article_html = urlopen(Article_url).read()
		Article_soap = BeautifulSoup(Article_html, 'lxml')
		print Article_url					
		#Name
		Article_name = Article_soap.find(id='titulo_producto')
		print Article_name.text

		#Price
		Article_Price_Div = Article_soap.find(id='pdpromos').div
		Article_Price_P = Article_Price_Div.findAll('p')
		if len(Article_Price_P) == 1:
			print Article_Price_P[0].text.replace(u'\n', u'').replace(u'Precio Promoción', '')
		else:
			print Article_Price_P[1].text.replace(u'\n', u'').replace(u'Precio Promoción', '')

		#Description
		Article_Description_Div = Article_soap.find(id='st_content_1')
		Article_Description = Article_Description_Div.div.div.text
		print Article_Description


getKeywords()
getArticles(link)