# -*- encoding: utf-8 -*-

from bs4 import BeautifulSoup
from urllib2 import urlopen

def getKeywords():

	# The URL of Sams Club Site that contains all categories
	url = 'http://www.cityclub.com.mx/store/default.aspx?p=7258'

	# Get the HTML description of the URL
	html = urlopen(url).read()

	# Creating a BeautifulSoup object from the HTML
	soap = BeautifulSoup(html, "lxml")

	# Get all the a tags that belong to div with id CategoriasTodas
	mainDivContainer = soap.find(id="qm0").contents

	# Create a list that will store all the Keywords that we will obtain
	finalList = []

	# Iterate over all the list to test the content of each element
	for i in mainDivContainer[1::2]:
		# Get a provitional list that have all the link on the iterated element
		provitionalList = i.findAll("a")

		# Iterate over all the elements of the provitional list to get acces to each link
		for j in provitionalList:

			# Exclude empty strings 
			if j.text != "":
				# Append to the final list the texto of the link in lowe case
				finalList.append(j.text.lower())
	
	# Convert the list to a set to clear all the repeated elements and then convert it back to list
	finalList = list(set(finalList))

	# Finally return the list which contains all the Keyword of the site
	return finalList