# -*- encoding: utf-8 -*-

from bs4 import BeautifulSoup
from urllib2 import urlopen
import json

# This method obtain al the posible Keywords for the articles
def getKeywords():

	# The URL of Sams Club Site that contains all categories
	url = 'http://www.sams.com.mx/all-categories.aspx'

	# Get the HTML description of the URL
	html = urlopen(url).read()

	# Creating a BeautifulSoup object from the HTML
	soap = BeautifulSoup(html, "lxml")

	# Get all the a tags that belong to div with id CategoriasTodas
	linkElements = soap.find(id="CategoriasTodo").contents

	# Create a list that will store all the Keywords that we will obtain
	finalList = []

	# Iterate over all the list to test the content of each div
	for i in linkElements:
		# Get a provitional list that have all the links on the div that we are iterating
		provitionalElement =  i.findAll("a")

		# Iterate over all the provitional list to get acces for each link
		for j in provitionalElement:

			# Push the text of the iterated link to the final list
			finalList.append(j.text.lower())

	# Convert the list to a set to clear all the repeated elements and then convert it back to list
	finalList = list(set(finalList))

	# Finally return the list which contains all the Keyword of the site
	return finalList

# This methos check if an article URL is valid or not and return a list with valid URL'site
def getValidURLs():

	urls = {}

	# Iterate on a list 900,000 times to prove wich URL exist
	for i in range(100000,1000000):

		# Declare a String with the format of the URL of the article
		url = "http://www.sams.com.mx/Detalle-de-articulo.aspx?upc="

		# Contatenate the number of iteration at the end of the URL
		url += str(i)

		# Get the HTML description of the URL
		html = urlopen(url).read()

		# Creating a BeautifulSoap object from the HTML
		soap = BeautifulSoup(html, "lxml")

		try:

			#Open an error page which start when the article doesnt exist
			soap.findAll("div", { "class" : "topslider" })[0]
		except:

			# Push to a dictionary the valud URL
			urls[str(i)] = url

			# Open the file and push the dictionary as a JSON format
			with open("salida.json", "w") as outfile:
				json.dump(urls, outfile, sort_keys=True, indent=4, separators=(',',':'))
			
	return None